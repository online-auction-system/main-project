﻿namespace OnlineAuctionSystem.DataAccess
{
    public static class StoredProcedures
    {
        public const string AddNewUser = "sp_Users_Insert_v0";
        public const string LoginUser = "sp_User_Login_v0";
        public const string GetUser = "sp_User_Get_v0";
        public const string GetUsers = "sp_Users_Get_By_Ids_v0";
        public const string GetAllNickNames = "sp_Users_Get_NickNames_v0";
    }
}
