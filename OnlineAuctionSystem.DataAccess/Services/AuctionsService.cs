﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OnlineAuctionSystem.Contracts.ExternalModels;
using OnlineAuctionSystem.Contracts.ExternalModels.Auctions;
using OnlineAuctionSystem.Contracts.Interfaces.Services;
using OnlineAuctionSystem.HttpLibrary;
using OnlineAuctionSystem.HttpLibrary.ContractResolvers;

namespace OnlineAuctionSystem.DataAccess.Services
{
    public class AuctionsService : BaseHttpClient, IAuctionsService
    {
        private readonly TimeSpan _defaultTimeOut;

        private readonly JsonSerializer _snakeSerializer = JsonSerializer.CreateDefault(new JsonSerializerSettings
            {ContractResolver = new SnakeCaseContractResolver()});

        public AuctionsService(ILogger<AuctionsService> logger)
            : base(logger)
        {
            CreateHttpClient();
            _defaultTimeOut = TimeSpan.FromSeconds(3);
        }

        protected override string ServiceName => "Auctions";
        protected override Entities.Services ServiceId => Entities.Services.Auctions;
        protected override string BaseUrl => "http://localhost:6636";

        public Task<List<ReadyAuctionExternalModel>> GetAllUserAuctionsAsync(int userId)
        {
            var url = "/auctions";
            var reqMsg = new HttpRequestMessage(HttpMethod.Get, url);
            reqMsg.Headers.Add("X-Authenticated-Userid", userId.ToString());
            return SendAsync<List<ReadyAuctionExternalModel>, SnakeCaseContractResolver>(reqMsg, _defaultTimeOut);
        }

        public async Task<bool> DeleteAuctionAsync(int userId, Guid auctionId)
        {
            var url = $"/auctions/{auctionId}";
            var reqMsg = new HttpRequestMessage(HttpMethod.Delete, url);
            reqMsg.Headers.Add("X-Authenticated-Userid", userId.ToString());
            var response = await OasSendAsync(reqMsg, _defaultTimeOut, new List<int> {404, 400}).ConfigureAwait(false);
            return response.IsSuccessStatusCode;
        }

        public Task<ReadyAuctionExternalModel> GetUserAuctionById(int userId, Guid auctionId)
        {
            var url = $"/auctions/{auctionId}";
            var reqMsg = new HttpRequestMessage(HttpMethod.Get, url);
            reqMsg.Headers.Add("X-Authenticated-Userid", userId.ToString());
            return SendAsync<ReadyAuctionExternalModel, SnakeCaseContractResolver>(reqMsg, _defaultTimeOut);
        }

        public Task<List<ReadyAuctionExternalModel>> GetAuctionsByIdsInternal(IList<Guid> auctionIds)
        {
            var ids = string.Join(',', auctionIds);
            var url = $"/internal/auctions?ids={ids}";
            var reqMsg = new HttpRequestMessage(HttpMethod.Get, url);
            return SendAsync<List<ReadyAuctionExternalModel>, SnakeCaseContractResolver>(reqMsg, _defaultTimeOut);
        }

        public async Task<ExternalServiceResponse<ReadyAuctionExternalModel>> TryAddAuctionAsync(
            NewAuctionExternalModel newAuction)
        {
            var content = CreateContent(newAuction, _snakeSerializer);
            var result = new ExternalServiceResponse<ReadyAuctionExternalModel>();
            var url = "/auctions/new";
            var reqMsg = new HttpRequestMessage(HttpMethod.Post, url) {Content = content};
            reqMsg.Headers.Add("X-Authenticated-Userid", newAuction.HolderId.ToString());
            var response = await OasSendAsync(reqMsg, _defaultTimeOut, new List<int> {400}).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                result.Success = true;
                result.Result = await response.Content
                    .ReadAsAsync<ReadyAuctionExternalModel, SnakeCaseContractResolver>()
                    .ConfigureAwait(false);
            }
            else
            {
                result.Success = false;
                result.ErrorInfo = await response.Content.ReadAsAsync<ErrorInfo, SnakeCaseContractResolver>()
                    .ConfigureAwait(false);
            }

            return result;
        }

        public async Task<ExternalServiceResponse<ReadyAuctionExternalModel>> TryEditAuctionAsync(NewAuctionExternalModel newAuction)
        {
            var content = CreateContent(newAuction, _snakeSerializer);
            var result = new ExternalServiceResponse<ReadyAuctionExternalModel>();
            var url = $"/auctions/{newAuction.AuctionId}/edit";
            var reqMsg = new HttpRequestMessage(HttpMethod.Post, url) { Content = content };
            reqMsg.Headers.Add("X-Authenticated-Userid", newAuction.HolderId.ToString());
            var response = await OasSendAsync(reqMsg, _defaultTimeOut, new List<int> { 400 }).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                result.Success = true;
                result.Result = await response.Content
                    .ReadAsAsync<ReadyAuctionExternalModel, SnakeCaseContractResolver>()
                    .ConfigureAwait(false);
            }
            else
            {
                result.Success = false;
                result.ErrorInfo = await response.Content.ReadAsAsync<ErrorInfo, SnakeCaseContractResolver>()
                    .ConfigureAwait(false);
            }

            return result;
        }
    }
}