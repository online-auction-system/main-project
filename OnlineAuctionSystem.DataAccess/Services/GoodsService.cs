﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OnlineAuctionSystem.Contracts.ExternalModels;
using OnlineAuctionSystem.Contracts.ExternalModels.Goods;
using OnlineAuctionSystem.Contracts.Interfaces.Services;
using OnlineAuctionSystem.HttpLibrary;
using OnlineAuctionSystem.HttpLibrary.ContractResolvers;


namespace OnlineAuctionSystem.DataAccess.Services
{
    public class GoodsService: BaseHttpClient, IGoodsService
    {
        private readonly TimeSpan _defaultTimeOut;

        private readonly JsonSerializer _snakeSerializer = JsonSerializer.CreateDefault(new JsonSerializerSettings
            { ContractResolver = new SnakeCaseContractResolver() });

        public GoodsService(ILogger<GoodsService> logger) : base(logger)
        {
            CreateHttpClient();
            _defaultTimeOut = TimeSpan.FromSeconds(3);
        }

        protected override string ServiceName => "Goods";
        protected override Entities.Services ServiceId => Entities.Services.Goods;
        protected override string BaseUrl => "http://localhost:7749";

        public Task<List<GoodExternalModel>> GetAllUserGoodsAsync(int userId)
        {
            var url = "/goods";
            var reqMsg = new HttpRequestMessage(HttpMethod.Get, url);
            reqMsg.Headers.Add("X-Authenticated-Userid", userId.ToString());
            return SendAsync<List<GoodExternalModel>, SnakeCaseContractResolver>(reqMsg, _defaultTimeOut);
        }

        public Task<List<GoodExternalModel>> GetGoodsByAuctionAsync(Guid auctionId)
        {
            var url = $"/internal/goods/on-auction/{auctionId}";
            var reqMsg = new HttpRequestMessage(HttpMethod.Get, url);
            return SendAsync<List<GoodExternalModel>, SnakeCaseContractResolver>(reqMsg, _defaultTimeOut);
        }

        public async Task<ExternalServiceResponse<GoodExternalModel>> TryAddGood(GoodExternalModel good)
        {
            var content = CreateContent(good, _snakeSerializer);
            var result = new ExternalServiceResponse<GoodExternalModel>();
            var url = "/goods/new";
            var reqMsg = new HttpRequestMessage(HttpMethod.Post, url) { Content = content };
            reqMsg.Headers.Add("X-Authenticated-Userid", good.CurrentOwnerId.ToString());
            var response = await OasSendAsync(reqMsg, _defaultTimeOut, new List<int> { 400, 403, 404 }).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                result.Success = true;
                result.Result = await response.Content
                    .ReadAsAsync<GoodExternalModel, SnakeCaseContractResolver>()
                    .ConfigureAwait(false);
            }
            else
            {
                result.Success = false;
                result.ErrorInfo = await response.Content.ReadAsAsync<ErrorInfo, SnakeCaseContractResolver>()
                    .ConfigureAwait(false);
            }

            return result;
        }

        public async Task<bool> DeleteGoodAsync(int userId, Guid goodId)
        {
            var url = $"/goods/{goodId}";
            var reqMsg = new HttpRequestMessage(HttpMethod.Delete, url);
            reqMsg.Headers.Add("X-Authenticated-Userid", userId.ToString());
            var response = await OasSendAsync(reqMsg, _defaultTimeOut, new List<int> { 404, 400 }).ConfigureAwait(false);
            return response.IsSuccessStatusCode;
        }

        public async Task<bool> RemoveGoodFromAuctionAsync(int userId, Guid goodId)
        {
            var url = $"/goods/{goodId}/remove";
            var reqMsg = new HttpRequestMessage(HttpMethod.Put, url);
            reqMsg.Headers.Add("X-Authenticated-Userid", userId.ToString());
            var response = await OasSendAsync(reqMsg, _defaultTimeOut, new List<int> { 404, 400, 403 }).ConfigureAwait(false);
            return response.IsSuccessStatusCode;
        }

        public Task<GoodExternalModel> GetUserGoodById(int userId, Guid goodId)
        {
            var url = $"/goods/{goodId}";
            var reqMsg = new HttpRequestMessage(HttpMethod.Get, url);
            reqMsg.Headers.Add("X-Authenticated-Userid", userId.ToString());
            return SendAsync<GoodExternalModel, SnakeCaseContractResolver>(reqMsg, _defaultTimeOut);
        }

        public async Task<ExternalServiceResponse<GoodExternalModel>> TryEditGoodAsync(GoodExternalModel model)
        {
            var content = CreateContent(model, _snakeSerializer);
            var result = new ExternalServiceResponse<GoodExternalModel>();
            var url = $"/goods/{model.GoodId}/edit";
            var reqMsg = new HttpRequestMessage(HttpMethod.Post, url) { Content = content };
            reqMsg.Headers.Add("X-Authenticated-Userid", model.CurrentOwnerId.ToString());
            var response = await OasSendAsync(reqMsg, _defaultTimeOut, new List<int> { 400 }).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                result.Success = true;
                result.Result = await response.Content
                    .ReadAsAsync<GoodExternalModel, SnakeCaseContractResolver>()
                    .ConfigureAwait(false);
            }
            else
            {
                result.Success = false;
                result.ErrorInfo = await response.Content.ReadAsAsync<ErrorInfo, SnakeCaseContractResolver>()
                    .ConfigureAwait(false);
            }

            return result;
        }
    }
}
