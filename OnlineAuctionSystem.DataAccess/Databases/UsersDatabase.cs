﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Dapper;
using OnlineAuctionSystem.Contracts.ExternalModels.DbModels;
using OnlineAuctionSystem.Contracts.Interfaces.Databases;
using OnlineAuctionSystem.DataAccessLibrary;

namespace OnlineAuctionSystem.DataAccess.Databases
{
    public class UsersDatabase: IUsersDatabase
    {
        private readonly BaseDataAccess _baseDataAccess;

        public UsersDatabase(BaseDataAccess baseDataAccess)
        {
            _baseDataAccess = baseDataAccess;
        }

        public Task<FullUserDbModel> TryRegisterUser(RegistrationDbModel insertModel)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@FirstName", insertModel.FirstName);
            parameters.Add("@SecondName", insertModel.SecondName);
            parameters.Add("@Patronymic", insertModel.Patronymic);
            parameters.Add("@DeliveryAddress", insertModel.DeliveryAddress);
            parameters.Add("@NickName", insertModel.NickName);
            parameters.Add("@MobilePhone", insertModel.MobilePhone);
            parameters.Add("@Email", insertModel.Email);
            parameters.Add("@EncryptedPassword", insertModel.EncryptedPassword);
            return _baseDataAccess.ExecuteStoredProcedureWithSingleResultAsync<FullUserDbModel>(StoredProcedures.AddNewUser, parameters);
        }

        public async Task<List<string>> GetAllUsedNicknames()
        {
             return (await _baseDataAccess.ExecuteStoredProcedureWithCollectionResultAsync<string>(StoredProcedures
                .GetAllNickNames)).ToList();
        }

        public Task<FullUserDbModel> TryLoginUser(LoginDbModel model)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@NickName", model.NickName);
            parameters.Add("@Password", model.EncryptedPassword);
            return _baseDataAccess.ExecuteStoredProcedureWithSingleResultAsync<FullUserDbModel>(StoredProcedures.LoginUser, parameters);
        }

        public Task<FullUserDbModel> GetUserModel(int userId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@UserId", userId);
            return _baseDataAccess.ExecuteStoredProcedureWithSingleResultAsync<FullUserDbModel>(
                StoredProcedures.GetUser, parameters);
        }

        public async Task<List<FullUserDbModel>> GetUserModelsAsync(IEnumerable<int> userIds)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@UserIds", MapIdsToJson(userIds));
            return (await _baseDataAccess.ExecuteStoredProcedureWithCollectionResultAsync<FullUserDbModel>(
                StoredProcedures.GetUsers, parameters)).ToList();
        }

        private static string MapIdsToJson(IEnumerable<int> ids)
        {
            return string.Join(",", ids);
        }
    }
}
