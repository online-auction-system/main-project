﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineAuctionSystem.Contracts.ExternalModels.Goods;
using OnlineAuctionSystem.Contracts.ExternalModels;

namespace OnlineAuctionSystem.Contracts.Interfaces.Services
{
    public interface IGoodsService
    {
        Task<List<GoodExternalModel>> GetAllUserGoodsAsync(int userId);
        Task<List<GoodExternalModel>> GetGoodsByAuctionAsync(Guid auctionId);
        Task<ExternalServiceResponse<GoodExternalModel>> TryAddGood(GoodExternalModel good);
        Task<bool> DeleteGoodAsync(int userId, Guid goodId);
        Task<bool> RemoveGoodFromAuctionAsync(int userId, Guid goodId);
        Task<GoodExternalModel> GetUserGoodById(int userId, Guid goodId);
        Task<ExternalServiceResponse<GoodExternalModel>> TryEditGoodAsync(GoodExternalModel model);
    }
}
