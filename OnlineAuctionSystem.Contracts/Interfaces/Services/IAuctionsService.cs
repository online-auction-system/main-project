﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineAuctionSystem.Contracts.ExternalModels;
using OnlineAuctionSystem.Contracts.ExternalModels.Auctions;

namespace OnlineAuctionSystem.Contracts.Interfaces.Services
{
    public interface IAuctionsService
    {
        Task<List<ReadyAuctionExternalModel>> GetAllUserAuctionsAsync(int userId);
        Task<bool> DeleteAuctionAsync(int userId, Guid auctionId);
        Task<ReadyAuctionExternalModel> GetUserAuctionById(int userId, Guid auctionId);
        Task<List<ReadyAuctionExternalModel>> GetAuctionsByIdsInternal(IList<Guid> auctionIds);
        Task<ExternalServiceResponse<ReadyAuctionExternalModel>> TryAddAuctionAsync(NewAuctionExternalModel newAuction);
        Task<ExternalServiceResponse<ReadyAuctionExternalModel>> TryEditAuctionAsync(NewAuctionExternalModel newAuction);
    }
}
