﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineAuctionSystem.Contracts.ExternalModels.DbModels;

namespace OnlineAuctionSystem.Contracts.Interfaces.Databases
{
    public interface IUsersDatabase
    {
        Task<FullUserDbModel> TryRegisterUser(RegistrationDbModel insertModel);
        Task<List<string>> GetAllUsedNicknames();
        Task<FullUserDbModel> TryLoginUser(LoginDbModel model);
        Task<FullUserDbModel> GetUserModel(int userId);
        Task<List<FullUserDbModel>> GetUserModelsAsync(IEnumerable<int> userIds);
    }
}
