﻿using System.Collections.Generic;

namespace OnlineAuctionSystem.Contracts.ExternalModels
{
    public class ExternalServiceResponse<T>
    {
        public bool Success { get; set; }
        public T Result { get; set; }
        public ErrorInfo ErrorInfo { get; set; }
    }

    public class ErrorInfo
    {
        public string Reason { get; set; }
        public string ErrorFields { get; set; }
        public List<ValidationError> ErrorsList { get; set; }
    }

    public class ValidationError
    {
        public string Property { get; set; }
        public string Reason { get; set; }
    }
}
