﻿using System;

namespace OnlineAuctionSystem.Contracts.ExternalModels.Auctions
{
    public class BaseAuctionExternalModel
    {
        public Guid AuctionId { get; set; }
        public int HolderId { get; set; }
        public DateTime StartTime { get; set; }
        public string Name { get; set; }
        public bool AllowAnonymous { get; set; }
        public int MinBuyerReputation { get; set; }
        public int MinSellerReputation { get; set; }
        public int MaxGoods { get; set; }
        public int BargainTime { get; set; }
        public int MaxParticipants { get; set; }
    }
}