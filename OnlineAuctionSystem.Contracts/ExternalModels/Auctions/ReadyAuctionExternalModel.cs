﻿using System;

namespace OnlineAuctionSystem.Contracts.ExternalModels.Auctions
{
    public class ReadyAuctionExternalModel : BaseAuctionExternalModel
    {
        public DateTime? EndTime { get; set; }
    }
}
