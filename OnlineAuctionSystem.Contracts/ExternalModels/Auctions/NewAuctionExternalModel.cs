﻿namespace OnlineAuctionSystem.Contracts.ExternalModels.Auctions
{
    public class NewAuctionExternalModel: BaseAuctionExternalModel
    {
        public NewAuctionExternalModel(ReadyAuctionExternalModel model)
        {
            MinBuyerReputation = model.MinBuyerReputation;
            MinSellerReputation = model.MinSellerReputation;
            MaxGoods = model.MinSellerReputation;
            BargainTime = model.BargainTime;
            MaxParticipants = model.MaxParticipants;
            HolderId = model.HolderId;
            Name = model.Name;
            StartTime = model.StartTime;
            AllowAnonymous = model.AllowAnonymous;
            AuctionId = model.AuctionId;
        }

        public NewAuctionExternalModel()
        {
        }
    }
}