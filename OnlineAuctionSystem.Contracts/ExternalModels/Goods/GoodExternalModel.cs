﻿using System;

namespace OnlineAuctionSystem.Contracts.ExternalModels.Goods
{
    public class GoodExternalModel
    {
        public Guid GoodId { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public Guid? AuctionId { get; set; }
        public int? PreviousOwnerId { get; set; }
        public int CurrentOwnerId { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public DateTime? LastTransferDate { get; set; }
    }
}
