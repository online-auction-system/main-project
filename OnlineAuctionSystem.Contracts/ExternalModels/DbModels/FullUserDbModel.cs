﻿using System;

namespace OnlineAuctionSystem.Contracts.ExternalModels.DbModels
{
    public class FullUserDbModel : RegistrationDbModel
    {
        public int UserId { get; set; }
        public DateTime CreationDate { get; set; }
        public decimal Balance { get; set; }
        public int BuyerReputation { get; set; }
        public int SellerReputation { get; set; }
    }
}
