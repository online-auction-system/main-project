﻿namespace OnlineAuctionSystem.Contracts.ExternalModels.DbModels
{
    public class LoginDbModel
    {
        public string NickName { get; set; }
        public string EncryptedPassword { get; set; }

        public LoginDbModel(string nick, string encryptedPass)
        {
            NickName = nick;
            EncryptedPassword = encryptedPass;
        }
    }
}
