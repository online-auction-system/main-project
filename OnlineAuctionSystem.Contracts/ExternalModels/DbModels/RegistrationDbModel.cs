﻿namespace OnlineAuctionSystem.Contracts.ExternalModels.DbModels
{
    public class RegistrationDbModel
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Patronymic { get; set; }
        public string DeliveryAddress { get; set; }
        public string NickName { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string EncryptedPassword { get; set; }

        public RegistrationDbModel(string firstName, string secondName, string patronymic, string deliveryAddress,
            string nickName, string mobilePhone, string email, string encryptedPass)
        {
            FirstName = firstName;
            SecondName = secondName;
            Patronymic = patronymic;
            DeliveryAddress = deliveryAddress;
            NickName = nickName;
            MobilePhone = mobilePhone;
            Email = email;
            EncryptedPassword = encryptedPass;
        }

        public RegistrationDbModel()
        {
        }
    }
}