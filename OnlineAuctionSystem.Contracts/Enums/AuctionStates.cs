﻿namespace OnlineAuctionSystem.Contracts.Enums
{
    public enum AuctionStates
    {
        Waiting = 0,
        StartingSoon = 1,
        Holding = 2,
        Finished = 3
    }
}
