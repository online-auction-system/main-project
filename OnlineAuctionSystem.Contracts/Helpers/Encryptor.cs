﻿namespace OnlineAuctionSystem.Contracts.Helpers
{
    public static class Encryptor
    {
        public static string EncryptPassword(string unencryptedPass)
        {
            var data = System.Text.Encoding.ASCII.GetBytes(unencryptedPass);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            return System.Text.Encoding.ASCII.GetString(data);
        }

    }
}
