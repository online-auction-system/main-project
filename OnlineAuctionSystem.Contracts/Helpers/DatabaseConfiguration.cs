﻿using System;

namespace OnlineAuctionSystem.Contracts.Helpers
{
    public class DatabaseConfiguration
    {
        public const string DatabaseConfigurationString = "Database=Online_Auction_System;Server=PROG-PC33;Integrated Security=False;uid=teamcity;pwd=teamcity;";
        public const int Timeout = 5;
    }
}