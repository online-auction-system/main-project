﻿using System.Security.Claims;
using System.Threading.Tasks;
using Blazored.SessionStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Online_Auction_System.Models;

namespace Online_Auction_System.Auth
{
    public class CustomAuthenticationStateProvider :  AuthenticationStateProvider
    {
        private readonly ISessionStorageService _sessionStorage;
        public CustomAuthenticationStateProvider(ISessionStorageService sessionStorage)
        {
            _sessionStorage = sessionStorage;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var userModel = await _sessionStorage.GetItemAsync<AuthorizedModel>("userModel");

            var identity = new ClaimsIdentity();
            if (userModel != null)
            {
                identity = new ClaimsIdentity( new []
                {
                    new Claim(ClaimTypes.GivenName, userModel.NickName),
                    new Claim(ClaimTypes.NameIdentifier, userModel.UserId.ToString()),
                    new Claim(ClaimTypes.Name, userModel.FirstName),
                    new Claim(ClaimTypes.Surname, userModel.SecondName)
                }, "api");
            }
            else
            {
                identity = new ClaimsIdentity(); 
            }

            var claimsPrincipal = new ClaimsPrincipal(identity);

            return new AuthenticationState(claimsPrincipal);
        }

        public async Task AuthenticateUser(AuthorizedModel model)
        {
            await _sessionStorage.SetItemAsync("userModel", model);

            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }

        public async Task LogUserOut()
        {
            await _sessionStorage.RemoveItemAsync("userModel");

            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }
    }
}
