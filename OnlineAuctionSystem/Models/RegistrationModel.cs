﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Online_Auction_System.Models
{
    public class RegistrationModel
    {
        [Required]
        [StringLength(50, ErrorMessage = "Name is too long.")]
        public string FirstName { get; set; }
        
        [Required]
        [StringLength(50, ErrorMessage = "Second name is too long.")]
        public string SecondName { get; set; }
        
        [StringLength(50, ErrorMessage = "Patronymic is too long.")]
        public string Patronymic { get; set; }

        [Required]
        [StringLength(150, ErrorMessage = "Delivery address is too long.")]
        public string DeliveryAddress { get; set; }

        //public string UserPicture { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "NickName is too long.")]
        public string NickName { get; set; }

        [Required]
        [Phone(ErrorMessage = "Enter a valid mobile phone please.")]
        public string MobilePhone { get; set; }
        
        [Required]
        [EmailAddress(ErrorMessage = "Enter a valid email please")]
        public string Email { get; set; }

        [Required]
        [MinLength(10, ErrorMessage = "Password must be at least 10 characters in length")]
        public string Password { get; set; }

        [Required]
        [PasswordsMatchAttribute(ComparisonProperty = "Password", ErrorMessage = "Password and confirmed password must match.")]
        [MinLength(10, ErrorMessage = "Password must be at least 10 characters in length")]
        public string ConfirmPassword { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class PasswordsMatchAttribute : ValidationAttribute
    {
        public string ComparisonProperty { get; set; }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var confirmedPassProperty = validationContext.ObjectType.GetProperty(ComparisonProperty);
            if (confirmedPassProperty == null)
            {
                throw new ArgumentException("Property with this name not found");
            }

            var confirmedPass = confirmedPassProperty.GetValue(validationContext.ObjectInstance).ToString();
            if (confirmedPass.Equals(value.ToString()))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(ErrorMessage);
        }
    }
}
