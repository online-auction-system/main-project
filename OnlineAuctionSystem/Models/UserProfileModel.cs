﻿using System;
using OnlineAuctionSystem.Contracts.ExternalModels.DbModels;

namespace Online_Auction_System.Models
{
    public class UserProfileModel
    {
        public UserProfileModel() {}

        public UserProfileModel(FullUserDbModel model)
        {
            UserId = model.UserId;
            FirstName = model.FirstName;
            SecondName = model.SecondName;
            Patronymic = model.Patronymic;
            Address = model.DeliveryAddress;
            NickName = model.NickName;
            MobilePhone = model.MobilePhone;
            CreationDate = model.CreationDate;
            Balance = model.Balance;
            Email = model.Email;
            SellerReputation = model.SellerReputation;
            BuyerReputation = model.BuyerReputation;
        }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Patronymic { get; set; }
        public string Address { get; set; }
        public string NickName { get; set; }
        public string MobilePhone { get; set; }
        public DateTime CreationDate { get; set; }
        public decimal Balance { get; set; }
        public string Email { get; set; }
        public int BuyerReputation { get; set; }
        public int SellerReputation { get; set; }
    }
}
