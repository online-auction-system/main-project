﻿namespace Online_Auction_System.Models
{
    public class AuthorizedModel
    {
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int UserId { get; set; }
    }
}
