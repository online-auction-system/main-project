﻿using System;
using OnlineAuctionSystem.Contracts.Enums;

namespace Online_Auction_System.Models
{
    public class AuctionModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int MinBuyerReputation { get; set; }
        public int MinSellerReputation { get; set; }
        public int MaxGoods { get; set; }
        public int BargainTime { get; set; }
        public int MaxParticipants { get; set; }
        public bool AllowAnonymous { get; set; }
        public AuctionStates CurrentAuctionState { get; set; }
    }
}
