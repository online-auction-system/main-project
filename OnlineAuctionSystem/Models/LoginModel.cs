﻿using System.ComponentModel.DataAnnotations;

namespace Online_Auction_System.Models
{
    public class LoginModel
    {
        [Required]
        public string NickName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
