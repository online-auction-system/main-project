﻿using System;

namespace Online_Auction_System.Models
{
    public class GoodModel
    {
        public Guid GoodId { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public Guid? AuctionId { get; set; }
        public int CurrentOwnerId { get; set; }
        public string CurrentOwner { get; set; }
        public string AuctionName { get; set; }
        public int? PreviousOwnerId { get; set; }
        public string PreviousOwner { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
    }
}
