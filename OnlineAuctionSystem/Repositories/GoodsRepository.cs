﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineAuctionSystem.Contracts.ExternalModels.Auctions;
using OnlineAuctionSystem.Contracts.ExternalModels.Goods;
using OnlineAuctionSystem.Contracts.Interfaces.Services;
using Online_Auction_System.Models;
using OnlineAuctionSystem.Contracts.ExternalModels;

namespace Online_Auction_System.Repositories
{
    public class GoodsRepository
    {
        private readonly IGoodsService _goodsService;
        private readonly AuctionsRepository _auctionsRepository;
        private readonly UsersRepository _usersRepository;

        public GoodsRepository(IGoodsService goodsService, AuctionsRepository auctionsRepository,
            UsersRepository usersRepository)
        {
            _goodsService = goodsService;
            _auctionsRepository = auctionsRepository;
            _usersRepository = usersRepository;
        }

        public async Task<List<GoodModel>> GetAllUserGoodsAsync(int userId)
        {
            var result = new List<GoodModel>();
            var serviceGoodModels = await _goodsService.GetAllUserGoodsAsync(userId);
            if (serviceGoodModels == null || serviceGoodModels.Count == 0)
            {
                return result;
            }

            var userIds = new HashSet<int> {userId};
            var auctionIds = new HashSet<Guid>();
            foreach (var good in serviceGoodModels)
            {
                if (good.PreviousOwnerId.HasValue)
                {
                    userIds.Add(good.PreviousOwnerId.Value);
                }

                if (good.AuctionId.HasValue)
                {
                    auctionIds.Add(good.AuctionId.Value);
                }
            }

            var usersDictionaryTask = GetUsersDictionaryByIds(userIds);
            var auctionsDictionaryTask = GetAuctionsDictionaryByIds(auctionIds);

            await Task.WhenAll(usersDictionaryTask, auctionsDictionaryTask);
            var auctionsDictionary = auctionsDictionaryTask.Result;
            var usersDictionary = usersDictionaryTask.Result;

            foreach (var good in serviceGoodModels)
            {
                var currentUserName = GetUserFullNameByModel(usersDictionary[good.CurrentOwnerId]);
                var previousUserName = good.PreviousOwnerId.HasValue
                    ? GetUserFullNameByModel(usersDictionary[good.PreviousOwnerId.Value])
                    : null;
                var auction = good.AuctionId.HasValue ? auctionsDictionary[good.AuctionId.Value] : null;
                result.Add(MapToFormModel(good, auction, currentUserName, previousUserName));
            }

            return result;
        }

        public async Task<List<GoodModel>> GetGoodsByAuctionAsync(int userId, Guid auctionId)
        {
            var result = new List<GoodModel>();

            var auction = await _auctionsRepository.GetUserAuctionById(userId, auctionId);
            if (auction == null)
            {
                return result;
            }

            var serviceGoodModels = await _goodsService.GetGoodsByAuctionAsync(auctionId);
            if (serviceGoodModels == null || serviceGoodModels.Count == 0)
            {
                return result;
            }

            var userIds = new HashSet<int>();
            foreach (var good in serviceGoodModels)
            {
                userIds.Add(good.CurrentOwnerId);
                if (good.PreviousOwnerId.HasValue)
                {
                    userIds.Add(good.PreviousOwnerId.Value);
                }
            }

            var usersDictionary = await GetUsersDictionaryByIds(userIds);
            result.AddRange(serviceGoodModels.Select(good => MapToFormModel(good, auction,
                GetUserFullNameByModel(usersDictionary[good.CurrentOwnerId]),
                good.PreviousOwnerId.HasValue
                    ? GetUserFullNameByModel(usersDictionary[good.PreviousOwnerId.Value])
                    : null)));

            return result;
        }

        public async Task<string> TryAddGoodAsync(GoodExternalModel good)
        {
            var result = await _goodsService.TryAddGood(good);
            return result.Success ? string.Empty : CreateUserErrorMessage(result.ErrorInfo);
        }

        public Task<bool> DeleteGood(int userId, Guid goodId)
        {
            return _goodsService.DeleteGoodAsync(userId, goodId);
        }

        public Task<bool> RemoveGoodFromAuction(int userId, Guid goodId)
        {
            return _goodsService.RemoveGoodFromAuctionAsync(userId, goodId);
        }

        public async Task<string> TryEditGoodAsync(GoodExternalModel newGood)
        {
            var result = await _goodsService.TryEditGoodAsync(newGood);
            return result.Success ? string.Empty : CreateUserErrorMessage(result.ErrorInfo);
        }

        public Task<GoodExternalModel> GetUserGoodById(int userId, Guid goodId)
        {
            return _goodsService.GetUserGoodById(userId, goodId);
        }

        private static string GetUserFullNameByModel(UserProfileModel user)
        {
            if (user == null)
            {
                return null;
            }

            var str = new StringBuilder($"{user.SecondName} {user.FirstName}");
            if (user.Patronymic != null)
            {
                str.Append($" {user.Patronymic}");
            }

            return str.ToString();
        }

        private async Task<Dictionary<int, UserProfileModel>> GetUsersDictionaryByIds(ICollection<int> userIds)
        {
            Dictionary<int, UserProfileModel> usersDictionary;
            if (userIds.Count > 0)
            {
                var users = await _usersRepository.GetUsersAsync(userIds);
                usersDictionary = new Dictionary<int, UserProfileModel>(users.Count);
                foreach (var user in users)
                {
                    usersDictionary.Add(user.UserId, user);
                }
            }
            else
            {
                usersDictionary = new Dictionary<int, UserProfileModel>();
            }

            return usersDictionary;
        }

        private async Task<Dictionary<Guid, ReadyAuctionExternalModel>> GetAuctionsDictionaryByIds(
            ICollection<Guid> auctionIds)
        {
            Dictionary<Guid, ReadyAuctionExternalModel> auctionsDictionary;
            if (auctionIds.Count > 0)
            {
                var auctions = await _auctionsRepository.GetAuctionsByIds(auctionIds.ToList());
                auctionsDictionary = new Dictionary<Guid, ReadyAuctionExternalModel>(auctions.Count);
                foreach (var auction in auctions)
                {
                    auctionsDictionary.Add(auction.AuctionId, auction);
                }
            }
            else
            {
                auctionsDictionary = new Dictionary<Guid, ReadyAuctionExternalModel>();
            }

            return auctionsDictionary;
        }

        private static GoodModel MapToFormModel(GoodExternalModel model, ReadyAuctionExternalModel auction,
            string currentOwner, string previousOwner)
        {
            return new GoodModel
            {
                AuctionId = model.AuctionId, Name = model.Name, CurrentOwner = currentOwner,
                CurrentOwnerId = model.CurrentOwnerId, Description = model.Description, GoodId = model.GoodId,
                PreviousOwnerId = model.PreviousOwnerId, PreviousOwner = previousOwner, Price = model.Price,
                Picture = model.Picture, AuctionName = auction?.Name
            };
        }

        private static string CreateUserErrorMessage(ErrorInfo errorInfo)
        {
            var errorMessage = new StringBuilder();

            if (errorInfo?.Reason != null)
            {
                errorMessage.Append(errorInfo.Reason);
            }

            if (errorInfo?.ErrorFields != null)
            {
                errorMessage.Append($": errors fields: {errorInfo.Reason}.");
            }

            if (errorInfo?.ErrorsList != null)
            {
                foreach (var error in errorInfo.ErrorsList)
                {
                    if (error != null)
                    {
                        errorMessage.Append($": {error.Reason}.");
                    }
                }
            }

            return errorMessage.ToString();
        }
    }
}