﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_Auction_System.Models;
using OnlineAuctionSystem.Contracts.Interfaces.Databases;

namespace Online_Auction_System.Repositories
{
    public class UsersRepository
    {
        private readonly IUsersDatabase _usersDatabase;

        public UsersRepository(IUsersDatabase usersDatabase)
        {
            _usersDatabase = usersDatabase;
        }

        public async Task<UserProfileModel> GetUserAsync(int userId)
        {
            var result = await _usersDatabase.GetUserModel(userId);
            return new UserProfileModel(result);
        }

        public async Task<List<UserProfileModel>> GetUsersAsync(IEnumerable<int> users)
        {
            var dbModels= await _usersDatabase.GetUserModelsAsync(users);
            return dbModels.Select(user => new UserProfileModel(user)).ToList();
        }
    }
}
