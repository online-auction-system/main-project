﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineAuctionSystem.Contracts.ExternalModels;
using OnlineAuctionSystem.Contracts.ExternalModels.DbModels;
using OnlineAuctionSystem.Contracts.Helpers;
using OnlineAuctionSystem.Contracts.Interfaces.Databases;
using Online_Auction_System.Models;

namespace Online_Auction_System.Repositories
{
    public class RegistrationRepository
    {
        private readonly IUsersDatabase _usersDatabase;
        private HashSet<string> _usedNickNames;

        public RegistrationRepository(IUsersDatabase usersDatabase)
        {
            _usersDatabase = usersDatabase;
        }

        public async Task InitAsync()
        {
            _usedNickNames = (await _usersDatabase.GetAllUsedNicknames()).ToHashSet();
        }

        public async Task<bool> TryRegisterUser(RegistrationModel model)
        {
            var ecnryptedPass = Encryptor.EncryptPassword(model.ConfirmPassword);
            var dbModel = new RegistrationDbModel(model.FirstName, model.SecondName, model.Patronymic,
                model.DeliveryAddress, model.NickName, model.MobilePhone, model.Email, ecnryptedPass);
            var registeredUser = await _usersDatabase.TryRegisterUser(dbModel);
            if (registeredUser != null)
            {
                _usedNickNames.Add(registeredUser.NickName);
            }

            return registeredUser != null;
        }

        public bool IsNicknameUnique(string nickName)
        {
            return !_usedNickNames.Contains(nickName);
        }
    }
}