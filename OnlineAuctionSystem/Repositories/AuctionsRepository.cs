﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineAuctionSystem.Contracts.ExternalModels;
using OnlineAuctionSystem.Contracts.ExternalModels.Auctions;
using OnlineAuctionSystem.Contracts.Interfaces.Services;
using Online_Auction_System.Models;

namespace Online_Auction_System.Repositories
{
    public class AuctionsRepository
    {
        private readonly IAuctionsService _auctionsService;

        public AuctionsRepository(IAuctionsService auctionsService)
        {
            _auctionsService = auctionsService;
        }

        public async Task<List<AuctionModel>> GetAllUserAuctionsAsync(int userId)
        {
            var result = new List<AuctionModel>();

            var serviceAuctionModels = await _auctionsService.GetAllUserAuctionsAsync(userId);
            if (serviceAuctionModels == null)
            {
                return result;
            }

            //Extremely slow approach. For any real data this needs to be rewritten 
            return serviceAuctionModels.OrderByDescending(auction => auction.StartTime)
                .ThenByDescending(auction => auction.EndTime).Select(MapToAuctionModel).ToList();
        }

        public Task<bool> DeleteAuctionAsync(int userId, Guid auctionId)
        {
            return _auctionsService.DeleteAuctionAsync(userId, auctionId);
        }

        public async Task<string> TryAddAuctionAsync(NewAuctionExternalModel newAuction)
        {
            var result = await _auctionsService.TryAddAuctionAsync(newAuction);
            return result.Success ? string.Empty : CreateUserErrorMessage(result.ErrorInfo);
        }

        public async Task<string> TryEditAuctionAsync(NewAuctionExternalModel newAuction)
        {
            var result = await _auctionsService.TryEditAuctionAsync(newAuction);
            return result.Success ? string.Empty : CreateUserErrorMessage(result.ErrorInfo);
        }

        public Task<ReadyAuctionExternalModel> GetUserAuctionById(int userId, Guid auctionId)
        {
            return _auctionsService.GetUserAuctionById(userId, auctionId);
        }

        public Task<List<ReadyAuctionExternalModel>> GetAuctionsByIds(IList<Guid> auctionIds)
        {
            return _auctionsService.GetAuctionsByIdsInternal(auctionIds);
        }

        private static string CreateUserErrorMessage(ErrorInfo errorInfo)
        {
            var errorMessage = new StringBuilder();

            if (errorInfo?.Reason != null)
            {
                errorMessage.Append(errorInfo.Reason);
            }
            if (errorInfo?.ErrorFields != null)
            {
                errorMessage.Append($": errors fields: {errorInfo.Reason}.");
            }
            if (errorInfo?.ErrorsList != null)
            {
                foreach (var error in errorInfo.ErrorsList)
                {
                    if (error != null)
                    {
                        errorMessage.Append($": {error.Reason}.");
                    }
                }
            }

            return errorMessage.ToString();
        }

        private static AuctionModel MapToAuctionModel(ReadyAuctionExternalModel externalModel)
        {
            return new AuctionModel
            {
                Name = externalModel.Name, StartTime = externalModel.StartTime, EndTime = externalModel.EndTime,
                AllowAnonymous = externalModel.AllowAnonymous, BargainTime = externalModel.BargainTime,
                MaxGoods = externalModel.MaxGoods, MaxParticipants = externalModel.MaxParticipants, 
                MinBuyerReputation = externalModel.MinBuyerReputation, MinSellerReputation = externalModel.MinSellerReputation,
                Id = externalModel.AuctionId
            };
        }
    }
}