﻿using System.Threading.Tasks;
using OnlineAuctionSystem.Contracts.ExternalModels;
using OnlineAuctionSystem.Contracts.ExternalModels.DbModels;
using OnlineAuctionSystem.Contracts.Helpers;
using OnlineAuctionSystem.Contracts.Interfaces.Databases;
using Online_Auction_System.Models;

namespace Online_Auction_System.Repositories
{
    public class LoginRepository
    {
        private readonly IUsersDatabase _usersDatabase;

        public LoginRepository(IUsersDatabase usersDatabase)
        {
            _usersDatabase = usersDatabase;
        }

        public async Task<AuthorizedModel> TryLogin(LoginModel model)
        {
            var encryptedPassword = Encryptor.EncryptPassword(model.Password);
            var loginModel = new LoginDbModel(model.NickName, encryptedPassword);
            var userModel = await _usersDatabase.TryLoginUser(loginModel);
            if (userModel == null)
            {
                return null;
            }

            return new AuthorizedModel
            {
                NickName = userModel.NickName, UserId = userModel.UserId, FirstName = userModel.FirstName,
                SecondName = userModel.SecondName
            };
        }
    }
}