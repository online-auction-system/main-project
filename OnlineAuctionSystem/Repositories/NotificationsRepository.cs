﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Online_Auction_System.Helpers.Rmq;
using Online_Auction_System.Helpers.Rmq.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Radzen;

namespace Online_Auction_System.Repositories
{
    public class NotificationsRepository : BaseRmqClient
    {
        public event Func<NotificationMessage, int, Task> RaiseNotificationAction;

        private static readonly JsonSerializer SnakeCaseSerializer = new JsonSerializer {CheckAdditionalContent = false, ContractResolver = new DefaultContractResolver
        {
            NamingStrategy = new SnakeCaseNamingStrategy
            {
                ProcessDictionaryKeys = true,
                OverrideSpecifiedNames = true
            }
        }};
        public NotificationsRepository()
        {
            Channel.ExchangeDeclare("oas.auctions", ExchangeType.Topic, autoDelete: true);
            CreateAndBindQueues();
        }

        private void CreateAndBindQueues()
        {
            var queueName = Channel.QueueDeclare("oas.auctions.new_goods_on_auctions");
            Channel.QueueBind(queue: queueName,"oas.auctions","new_good");
            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += RaiseNotification;
            Channel.BasicConsume(queueName, true, consumer);
        }

        private void RaiseNotification(object? obj, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            using var jsonStream = new MemoryStream(body, false);
            using var streamReader = new StreamReader(jsonStream);
            using JsonReader jsonReader = new JsonTextReader(streamReader);
            var rmqModel = SnakeCaseSerializer.Deserialize<NewGoodOnAuctionRmqModel>(jsonReader);
            var notification = CreateNotification(rmqModel);
            if (notification != null)
            {
                RaiseNotificationAction?.Invoke(notification, rmqModel.AuctionHolderId);
            }
        }

        private NotificationMessage CreateNotification(NewGoodOnAuctionRmqModel model)
        {
            if (model == null)
            {
                return null;
            }
            var infoMessage = $"A new good ({model.GoodName}) was posted on your auction ({model.AuctionName}).";
            return new NotificationMessage{Summary = "New good on your auction!", Detail = infoMessage, Severity = NotificationSeverity.Info, Duration = 4000};
        }
    }
}
