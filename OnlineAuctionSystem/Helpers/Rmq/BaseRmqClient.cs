﻿using System;
using RabbitMQ.Client;

namespace Online_Auction_System.Helpers.Rmq
{
    public class BaseRmqClient : IDisposable
    {
        protected readonly IModel Channel;
        protected IConnection Connection;

        public BaseRmqClient()
        {
            var factory = new ConnectionFactory { HostName = "localhost" };
            Connection = factory.CreateConnection();
            Channel = Connection.CreateModel();
        }

        public void Dispose()
        {
            Channel?.Dispose();
            Connection?.Dispose();
        }
    }
}