﻿using System;

namespace Online_Auction_System.Helpers.Rmq.Models
{
    public class NewGoodOnAuctionRmqModel
    {
        public Guid GoodId { get; set; }
        public string GoodName { get; set; }
        public int GoodOwnerId { get; set; }
        public Guid AuctionId { get; set; }
        public string AuctionName { get; set; }
        public int AuctionHolderId { get; set; }
        public DateTime PostTime { get; set; }
    }
}