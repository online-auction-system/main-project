﻿using OnlineAuctionSystem.Contracts.ExternalModels.Auctions;

namespace Online_Auction_System.Helpers
{
    public static class Mapper
    {
        public static NewAuctionExternalModel MapExternalModelToInnerModel(ReadyAuctionExternalModel model)
        {
            return model == null ? null : new NewAuctionExternalModel(model);
        }
    }
}
