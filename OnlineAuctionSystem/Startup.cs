using Blazored.SessionStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OnlineAuctionSystem.Contracts.Interfaces.Databases;
using OnlineAuctionSystem.Contracts.Interfaces.Services;
using OnlineAuctionSystem.DataAccess;
using OnlineAuctionSystem.DataAccessLibrary;
using Online_Auction_System.AppInit;
using Online_Auction_System.Auth;
using OnlineAuctionSystem.DataAccess.Databases;
using OnlineAuctionSystem.DataAccess.Services;
using Online_Auction_System.Repositories;
using Radzen;

namespace Online_Auction_System
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddScoped<AuthenticationStateProvider, CustomAuthenticationStateProvider>();
            services.AddBlazoredSessionStorage();
            services.AddSingleton<BaseDataAccess>();
            services.AddSingleton<RegistrationRepository>();
            services.AddSingleton<LoginRepository>();
            services.AddSingleton<AuctionsRepository>();
            services.AddSingleton<GoodsRepository>();
            services.AddSingleton<UsersRepository>();
            services.AddSingleton<NotificationsRepository>();
            services.AddSingleton<IDatabaseConfig, DbConfig>();
            services.AddSingleton<IUsersDatabase, UsersDatabase>();
            services.AddSingleton<IAuctionsService, AuctionsService>();
            services.AddSingleton<IGoodsService, GoodsService>();
            services.AddSingleton<ApplicationInitialization>();
            services.AddSingleton<NotificationService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });

            var appInit = app.ApplicationServices.GetRequiredService<ApplicationInitialization>();
            appInit.InitAsync().Wait();
        }
    }
}
