﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Online_Auction_System.Repositories;

namespace Online_Auction_System.AppInit
{
    public class ApplicationInitialization
    {
        private readonly RegistrationRepository _registrationRepository;

        public ApplicationInitialization(RegistrationRepository registrationRepository)
        {
            _registrationRepository = registrationRepository;
        }

        public async Task InitAsync()
        {
            await _registrationRepository.InitAsync();
        }
    }
}
