CREATE PROCEDURE [dbo].[sp_Auction_Get_With_Permissions_v0]
	@AuctionId UNIQUEIDENTIFIER,
	@UserId INT
AS
BEGIN
	 SELECT AuctionId,
		 HolderId,
		 StartTime,
		 EndTime, 
		 MinBuyerReputation, 
		 MinSellerReputation, 
		 MaxGoods, 
		 BargainTime, 
		 MaxParticipants, 
		 Name,
		 BlackList,
		 AllowAnonymous FROM [dbo].[Auctions] WHERE AuctionId = @AuctionId AND HolderId = @UserId;
END