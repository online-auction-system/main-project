CREATE PROCEDURE [dbo].[sp_Users_Get_By_Ids_v0]
	@UserIds VARCHAR(MAX)
AS
BEGIN

	DECLARE @SellerReputations TABLE (UserId INT, SellerReputation INT);
	DECLARE @BuyerReputation TABLE (UserId INT, BuyerReputation INT);

	INSERT INTO @SellerReputations (UserId, SellerReputation)
		SELECT UserId, Reputation FROM [dbo].[UserInfo] WHERE UserId IN (SELECT CAST(VALUE AS INT) as Id from STRING_SPLIT(@UserIds, ',')) AND RoleId = 1
	INSERT INTO @BuyerReputation (UserId, BuyerReputation)
		SELECT UserId, Reputation FROM [dbo].[UserInfo] WHERE UserId IN (SELECT CAST(VALUE AS INT) as Id from STRING_SPLIT(@UserIds, ',')) AND RoleId = 2

	 SELECT Id AS UserId,
	FirstName,
	SecondName,
	Patronymic, 
	DeliveryAddress, 
	UserName AS NickName, 
	MobilePhone,
	Email, 
	CreationDate, 
	Balance,
	BuyerReputation,
	SellerReputation FROM 
	[dbo].[Users] us INNER JOIN @SellerReputations sr ON us.Id = sr.UserId
	INNER JOIN @BuyerReputation br ON us.Id = br.UserId
	  WHERE Id IN (SELECT CAST(VALUE AS INT) as UserId from STRING_SPLIT(@UserIds, ','))
END
GO