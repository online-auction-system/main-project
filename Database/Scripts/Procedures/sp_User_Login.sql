USE Online_Auction_System
GO

CREATE PROCEDURE [dbo].[sp_User_Login_v0]
	@NickName NVARCHAR(MAX),
	@Password NVARCHAR(MAX)
AS
BEGIN
	SELECT Id AS UserId,
	FirstName,
	SecondName,
	Patronymic, 
	DeliveryAddress, 
	UserName AS NickName, 
	MobilePhone,
	Email, 
	CreationDate, 
	Balance FROM [dbo].[Users] WHERE UserName = @NickName AND Password = @Password;
END