CREATE PROCEDURE [dbo].[sp_Goods_Get_v0]
	@OwnerId INT
AS
BEGIN
	 SELECT Id AS GoodId,
	 Price AS Price,
	 Picture AS Picture,
	 Description AS Description, 
	 AuctionId AS AuctionId, 
	 PreviousOwnerId AS PreviousOwnerId, 
	 CurrentOwnerId AS CurrentOwnerId, 
	 Name AS Name, 
	 LastTransferDate AS LastTransferDate FROM dbo.Goods WHERE CurrentOwnerId = @OwnerId
END
GO