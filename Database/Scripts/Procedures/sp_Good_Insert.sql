CREATE PROCEDURE [dbo].[sp_Good_Insert_v0]
	@Price MONEY,
	@Picture NVARCHAR(MAX),
	@Description NVARCHAR(MAX),
	@AuctionId UNIQUEIDENTIFIER,
	@PreviousOwnerId INT,
	@CurrentOwnerId INT,
	@Name NVARCHAR(MAX),
	@LastTransferDate DATETIME,
	@MaxGoodsOnAuction INT
AS
BEGIN

	DECLARE @GoodsIds TABLE
    (
        GoodId uniqueidentifier
    )

	IF @MaxGoodsOnAuction IS NOT NULL
		BEGIN
			DECLARE @GoodsOnAuction INT
			SELECT @GoodsOnAuction = COUNT(*) FROM [dbo].[Goods] WHERE @AuctionId IS NOT NULL AND AuctionId = @AuctionId
			IF @GoodsOnAuction >= @MaxGoodsOnAuction
				RETURN
		END
	
	INSERT INTO dbo.Goods 
	(Price,
	 Picture,
	 Description, 
	 AuctionId, 
	 PreviousOwnerId, 
	 CurrentOwnerId, 
	 Name, 
	 LastTransferDate)
	OUTPUT inserted.Id INTO @GoodsIds
	VALUES 
	(@Price,
	 @Picture,
	 @Description,
	 @AuctionId,
	 @PreviousOwnerId,
	 @CurrentOwnerId,
	 @Name,
	 @LastTransferDate)

	 DECLARE @GoodId UNIQUEIDENTIFIER
     SELECT TOP 1 @GoodId = GoodId FROM @GoodsIds

	 SELECT Id AS GoodId,
	 Price AS Price,
	 Picture AS Picture,
	 Description AS Description, 
	 AuctionId AS AuctionId, 
	 PreviousOwnerId AS PreviousOwnerId, 
	 CurrentOwnerId AS CurrentOwnerId, 
	 Name AS Name, 
	 LastTransferDate AS LastTransferDate FROM dbo.Goods WHERE Id = @GoodId
END
GO
