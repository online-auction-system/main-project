CREATE PROCEDURE [dbo].[sp_Auction_Delete_v0]
	@AuctionId UNIQUEIDENTIFIER,
	@UserId INT
AS
BEGIN

	DECLARE @AuctionIds TABLE
		(
			AuctionId UNIQUEIDENTIFIER
		)
	 DELETE FROM [dbo].[Auctions]
	 OUTPUT deleted.AuctionId INTO @AuctionIds
	 WHERE AuctionId = @AuctionId AND HolderId = @UserId

	 DECLARE @DeletedAuctionId UNIQUEIDENTIFIER
     SELECT TOP 1 @DeletedAuctionId = AuctionId FROM @AuctionIds

	 IF @DeletedAuctionId IS NOT NULL
		SELECT 1
	 ELSE
		SELECT 0
END