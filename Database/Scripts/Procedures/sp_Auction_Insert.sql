CREATE PROCEDURE [dbo].[sp_Auction_Insert_v0]
	@Name NVARCHAR(MAX),
	@BargainTime INT,
	@EndTime DATETIME,
	@HolderId INT,
	@MaxGoods INT,
	@MaxParticipants INT,
	@StartTime DATETIME,
	@MinBuyerReputation INT,
	@MinSellerReputation INT,
	@AllowAnonymous BIT 
AS
BEGIN

	DECLARE @AuctionIds TABLE
    (
        AuctionId uniqueidentifier
    )

	INSERT INTO dbo.Auctions 
	(HolderId,
	 StartTime,
	 EndTime, 
	 MinBuyerReputation, 
	 MinSellerReputation, 
	 MaxGoods, 
	 BargainTime, 
	 MaxParticipants, 
	 Name,
	 AllowAnonymous)
	OUTPUT inserted.AuctionId INTO @AuctionIds
	VALUES 
	(@HolderId,
	 @StartTime,
	 @EndTime,
	 @MinBuyerReputation,
	 @MinSellerReputation,
	 @MaxGoods,
	 @BargainTime,
	 @MaxParticipants,
	 @Name,
	 @AllowAnonymous)

	 DECLARE @AuctionId uniqueidentifier
     SELECT TOP 1 @AuctionId = AuctionId FROM @AuctionIds

	 SELECT AuctionId AS AuctionId,
		 HolderId AS HolderId,
		 StartTime AS StartTime,
		 EndTime AS EndTime, 
		 MinBuyerReputation AS MinBuyerReputation, 
		 MinSellerReputation AS MinSellerReputation, 
		 MaxGoods AS MaxGoods, 
		 BargainTime AS BargainTime, 
		 MaxParticipants AS MaxParticipants,
		 AllowAnonymous AS AllowAnonymous,
		 Name AS Name FROM dbo.Auctions WHERE AuctionId = @AuctionId
END
GO
