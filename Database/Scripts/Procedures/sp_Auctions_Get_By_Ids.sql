CREATE PROCEDURE [dbo].[sp_Auctions_Get_By_Ids_v0]
	@AuctionIds VARCHAR(MAX)
AS
BEGIN
	 SELECT AuctionId,
		 HolderId,
		 StartTime,
		 EndTime, 
		 MinBuyerReputation, 
		 MinSellerReputation, 
		 MaxGoods, 
		 BargainTime, 
		 MaxParticipants, 
		 Name,
		 BlackList,
		 AllowAnonymous FROM dbo.Auctions WHERE AuctionId IN (SELECT CAST(VALUE AS UNIQUEIDENTIFIER) as AuctionId from STRING_SPLIT(@AuctionIds, ','))
END
GO