CREATE PROCEDURE [dbo].[sp_Good_Delete_v0]
	@GoodId UNIQUEIDENTIFIER,
	@UserId INT
AS
BEGIN

	DECLARE @GoodIds TABLE
		(
			GoodId UNIQUEIDENTIFIER
		)
	 DELETE FROM [dbo].[Goods]
	 OUTPUT deleted.Id INTO @GoodIds
	 WHERE Id = @GoodId AND CurrentOwnerId = @UserId

	 DECLARE @DeletedGoodId UNIQUEIDENTIFIER
     SELECT TOP 1 @DeletedGoodId = GoodId FROM @GoodIds

	 IF @DeletedGoodId IS NOT NULL
		SELECT 1
	 ELSE
		SELECT 0
END