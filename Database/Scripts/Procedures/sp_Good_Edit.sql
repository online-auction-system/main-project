CREATE PROCEDURE [dbo].[sp_Good_Edit_v0]
	@GoodId UNIQUEIDENTIFIER,
	@Price MONEY,
	@Picture NVARCHAR(MAX),
	@Description NVARCHAR(MAX),
	@AuctionId UNIQUEIDENTIFIER,
	@CurrentOwnerId INT,
	@Name NVARCHAR(MAX),
	@MaxGoodsOnAuction INT
AS
BEGIN
	IF @MaxGoodsOnAuction IS NOT NULL
		BEGIN
			DECLARE @GoodsOnAuction INT
			SELECT @GoodsOnAuction = COUNT(*) FROM [dbo].[Goods] WHERE @AuctionId IS NOT NULL AND AuctionId = @AuctionId
			IF @GoodsOnAuction >= @MaxGoodsOnAuction
				RETURN
		END

	DECLARE @GoodsIds TABLE
    (
        GoodId uniqueidentifier
    )
	
	UPDATE [dbo].[Goods]
	SET Price = @Price,
	Picture = @Picture,
	Description = @Description,
	AuctionId = @AuctionId,
	CurrentOwnerId = @CurrentOwnerId,
	Name = @Name 
	OUTPUT INSERTED.Id INTO @GoodsIds
	WHERE Id = @GoodId AND CurrentOwnerId = @CurrentOwnerId

	DECLARE @UpdatedGoodId UNIQUEIDENTIFIER
     SELECT TOP 1 @UpdatedGoodId = GoodId FROM @GoodsIds

	SELECT Id AS GoodId,
	Price AS Price,
	Picture AS Picture,
	Description AS Description, 
	AuctionId AS AuctionId, 
	PreviousOwnerId AS PreviousOwnerId, 
	CurrentOwnerId AS CurrentOwnerId, 
	Name AS Name, 
	LastTransferDate AS LastTransferDate FROM dbo.Goods WHERE Id = @UpdatedGoodId
END
GO
