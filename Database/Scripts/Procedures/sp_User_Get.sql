CREATE PROCEDURE [dbo].[sp_User_Get_v0]
	@UserId INT
AS
BEGIN

	DECLARE @SellerReputation INT;
	DECLARE @BuyerReputation INT;

	SELECT @BuyerReputation = MAX(Reputation) FROM [dbo].[UserInfo] WHERE UserId = @UserId AND RoleId = 1
	SELECT @SellerReputation = MAX(Reputation) FROM [dbo].[UserInfo] WHERE UserId = @UserId AND RoleId = 2

	SELECT Id AS UserId,
	FirstName,
	SecondName,
	Patronymic, 
	DeliveryAddress, 
	UserName AS NickName, 
	MobilePhone,
	Email, 
	CreationDate, 
	Balance,
	@BuyerReputation,
	@SellerReputation FROM [dbo].[Users] WHERE Id = @UserId;
END