CREATE PROCEDURE [dbo].[sp_Good_Get_v0]
	@GoodId UNIQUEIDENTIFIER
AS
BEGIN
	 SELECT Id AS GoodId,
	 Price AS Price,
	 Picture AS Picture,
	 Description AS Description, 
	 AuctionId AS AuctionId, 
	 PreviousOwnerId AS PreviousOwnerId, 
	 CurrentOwnerId AS CurrentOwnerId, 
	 Name AS Name, 
	 LastTransferDate AS LastTransferDate FROM dbo.Goods WHERE Id = @GoodId
END
GO