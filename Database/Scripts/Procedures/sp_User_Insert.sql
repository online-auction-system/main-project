USE Online_Auction_System
GO

CREATE OR ALTER PROCEDURE [dbo].[sp_Users_Insert_v0]
	@FirstName NVARCHAR(50),
	@SecondName NVARCHAR(50),
	@Patronymic NVARCHAR(50),
    @DeliveryAddress NVARCHAR(150),
	@NickName NVARCHAR(50),
	@MobilePhone NVARCHAR(MAX),
	@Email NVARCHAR(MAX),
	@EncryptedPassword NVARCHAR(MAX)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION [InsertUser];

			DECLARE @UsersIds TABLE
			(
				UserId INT
			)

			INSERT INTO dbo.Users 
			(FirstName,
			 SecondName,
			 Patronymic, 
			 DeliveryAddress, 
			 UserName, 
			 MobilePhone, 
			 CreationDate, 
			 Balance,
			 Email,
			 Password)
			OUTPUT inserted.Id INTO @UsersIds
			VALUES 
			(@FirstName,
			 @SecondName,
			 @Patronymic, 
			 @DeliveryAddress, 
			 @NickName, 
			 @MobilePhone, 
			 GETDATE(), 
			 0,
			 @Email,
			 @EncryptedPassword)

			 DECLARE @UserId INT
			 SELECT TOP 1 @UserId = UserId FROM @UsersIds

			 INSERT INTO dbo.UserInfo
			 (UserId,
			 RoleId,
			 Reputation,
			 RoleActionsCount,
			 LastActionDate)
			 VALUES
			 (@UserId,
			 1,
			 0,
			 0,
			 NULL),
			 (@UserId,
			 2,
			 0,
			 0,
			 NULL)
		COMMIT TRANSACTION [InsertUser];
		
	END TRY

	BEGIN CATCH
			
		IF XACT_STATE() <> 0
			ROLLBACK TRANSACTION [InsertUser];
		THROW;
	END CATCH;

	SELECT Id AS UserId,
	FirstName,
	SecondName,
	Patronymic, 
	DeliveryAddress, 
	UserName AS NickName, 
	MobilePhone,
	Email, 
	CreationDate, 
	Balance FROM dbo.Users WHERE Id = @UserId
END