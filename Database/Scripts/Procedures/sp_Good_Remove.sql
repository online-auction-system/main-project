CREATE PROCEDURE [dbo].[sp_Good_Remove_v0]
	@GoodId UNIQUEIDENTIFIER
AS
BEGIN

	DECLARE @GoodIds TABLE
		(
			GoodId UNIQUEIDENTIFIER
		)
	 UPDATE [dbo].[Goods]
	 SET AuctionId = NULL 
     OUTPUT INSERTED.Id INTO @GoodIds
	 WHERE Id = @GoodId

	 DECLARE @RemovedGoodId UNIQUEIDENTIFIER
     SELECT TOP 1 @RemovedGoodId = GoodId FROM @GoodIds

	 IF @RemovedGoodId IS NOT NULL
		SELECT 1
	 ELSE
		SELECT 0
END