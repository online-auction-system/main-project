CREATE PROCEDURE [dbo].[sp_Auction_Edit_v0]
	@AuctionId UNIQUEIDENTIFIER,
	@Name NVARCHAR(MAX),
	@BargainTime INT,
	@EndTime DATETIME,
	@HolderId INT,
	@MaxGoods INT,
	@MaxParticipants INT,
	@StartTime DATETIME,
	@MinBuyerReputation INT,
	@MinSellerReputation INT,
	@AllowAnonymous BIT 
AS
BEGIN

	DECLARE @FoundItems INT
	SELECT @FoundItems = COUNT(*) FROM [dbo].[Auctions] WHERE AuctionId = @AuctionId AND HolderId = @HolderId AND StartTime > DATEADD(MINUTE, 30, GETDATE()) 
	
	IF (@FoundItems = 0)
		RETURN

	UPDATE dbo.Auctions 
		SET
			StartTime = @StartTime,
			EndTime = @EndTime, 
			MinBuyerReputation = @MinBuyerReputation, 
			MinSellerReputation = @MinSellerReputation, 
			MaxGoods = @MaxGoods, 
			BargainTime = @BargainTime, 
			MaxParticipants = @MaxParticipants, 
			Name = @Name,
			AllowAnonymous = @AllowAnonymous
		WHERE AuctionId = @AuctionId;

	SELECT AuctionId AS AuctionId,
		HolderId AS HolderId,
		StartTime AS StartTime,
		EndTime AS EndTime, 
		MinBuyerReputation AS MinBuyerReputation, 
		MinSellerReputation AS MinSellerReputation, 
		MaxGoods AS MaxGoods, 
		BargainTime AS BargainTime, 
		MaxParticipants AS MaxParticipants,
		AllowAnonymous AS AllowAnonymous,
		Name AS Name FROM dbo.Auctions WHERE AuctionId = @AuctionId
END
