CREATE PROCEDURE [dbo].[sp_Good_Post_v0]
	@GoodId UNIQUEIDENTIFIER,
	@AuctionId UNIQUEIDENTIFIER,
	@MaxGoods INT
AS
BEGIN

	IF @MaxGoods IS NOT NULL
		BEGIN
			DECLARE @GoodsOnAuction INT
			SELECT @GoodsOnAuction = COUNT(*) FROM [dbo].[Goods] WHERE @AuctionId IS NOT NULL AND AuctionId = @AuctionId
			IF @GoodsOnAuction >= @MaxGoods
				RETURN
		END

	 UPDATE dbo.Goods SET AuctionId = @AuctionId OUTPUT INSERTED.Id WHERE Id = @GoodId
END