CREATE PROCEDURE [dbo].[sp_Good_Get_With_Permissions_v0]
	@UserId INT,
	@GoodId UNIQUEIDENTIFIER
AS
BEGIN
	 SELECT Id AS GoodId,
	 Price AS Price,
	 Picture AS Picture,
	 Description AS Description, 
	 AuctionId AS AuctionId, 
	 PreviousOwnerId AS PreviousOwnerId, 
	 CurrentOwnerId AS CurrentOwnerId, 
	 Name AS Name, 
	 LastTransferDate AS LastTransferDate FROM [dbo].[Goods] WHERE Id = @GoodId AND CurrentOwnerId = @UserId;
END