CREATE PROCEDURE [dbo].[sp_Auctions_Get_v0]
	@UserId INT
AS
BEGIN
	 SELECT AuctionId,
		 HolderId,
		 StartTime,
		 EndTime, 
		 MinBuyerReputation, 
		 MinSellerReputation, 
		 MaxGoods, 
		 BargainTime, 
		 MaxParticipants, 
		 Name,
		 BlackList,
		 AllowAnonymous FROM dbo.Auctions WHERE HolderId = @UserId
END
GO