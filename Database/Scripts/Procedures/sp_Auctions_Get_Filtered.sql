CREATE OR ALTER PROCEDURE [dbo].[sp_Auctions_Get_Filtered_v0]
	@UserId INT,
	@UserRole INT,
	@UserReputation INT,
	@AuctionStartTime DATETIME
AS
BEGIN
	 SELECT AuctionId,
		 HolderId,
		 StartTime,
		 EndTime, 
		 MinBuyerReputation, 
		 MinSellerReputation, 
		 MaxGoods, 
		 BargainTime, 
		 MaxParticipants
		 Name,
		 BlackList,
		 AllowAnonymous FROM dbo.Auctions WHERE (@UserId != 0 OR AllowAnonymous = 1) AND
		 (@UserRole IS NULL OR (@UserRole = 1 AND MinBuyerReputation >= @UserReputation) OR (@UserRole = 2 AND MinSellerReputation >= @UserReputation))
END
GO