USE Online_Auction_System
GO

CREATE PROCEDURE [dbo].[sp_Goods_Get_By_Auction_v0]
	@AuctionId UNIQUEIDENTIFIER
AS
BEGIN
	 SELECT Id AS GoodId,
	 Price AS Price,
	 Picture AS Picture,
	 Description AS Description, 
	 AuctionId AS AuctionId, 
	 PreviousOwnerId AS PreviousOwnerId, 
	 CurrentOwnerId AS CurrentOwnerId, 
	 Name AS Name, 
	 LastTransferDate AS LastTransferDate FROM dbo.Goods WHERE AuctionId = @AuctionId
END
GO