USE Online_Auction_System 
GO

CREATE TABLE UserInfo 
	(UserId INT NOT NULL,
	RoleId INT NOT NULL,
	Reputation INT NOT NULL,
	RoleActionsCount INT NOT NULL,
	LastActionDate DATETIME NULL,
	CONSTRAINT UI_pk PRIMARY KEY (UserId, RoleId))