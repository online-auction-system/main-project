USE Online_Auction_System 
GO

CREATE TABLE Goods 
	(Id UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
	Price MONEY NOT NULL,
	Picture nvarchar(max) NULL,
	Description nvarchar(max) NULL,
	AuctionId UNIQUEIDENTIFIER NULL,
	PreviousOwnerId INT NULL,
	CurrentOwnerId INT NOT NULL,
	Name nvarchar(max) NOT NULL,
	LastTransferDate DATETIME NULL,
	CONSTRAINT GS_pk PRIMARY KEY (Id))