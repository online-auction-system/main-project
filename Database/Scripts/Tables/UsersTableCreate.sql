USE Online_Auction_System 
GO

CREATE TABLE Users 
	(Id INT NOT NULL IDENTITY(1,1),
	FirstName nvarchar(50) NOT NULL,
	SecondName nvarchar(70) NOT NULL,
	Patronymic nvarchar(70) NULL,
	DeliveryAddress nvarchar(max) NOT NULL,
	UserName nvarchar(50) NOT NULL,
	MobilePhone nvarchar(15) NOT NULL,
	CreationDate Date NOT NULL,
	Balance money NOT NULL,
	Email nvarchar(50) NOT NULL,
	Password nvarchar(max) NOT NULL
	CONSTRAINT US_pk PRIMARY KEY (Id),
	CONSTRAINT US_UserName UNIQUE(UserName))