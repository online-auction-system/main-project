USE Online_Auction_System 
GO

CREATE TABLE Auctions 
	(AuctionId UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
	HolderId INT NOT NULL,
	StartTime Datetime NOT NULL,
	EndTime Datetime NULL,
	MinBuyerReputation INT NULL,
	MinSellerReputation INT NULL,
	MaxGoods INT NULL,
	BargainTime INT NOT NULL,
	MaxParticipants INT NULL,
	Name NVARCHAR(MAX) NOT NULL,
	BlackList NVARCHAR(MAX) NULL,
	AllowAnonymous BIT NOT NULL DEFAULT(1)
	CONSTRAINT AS_pk PRIMARY KEY (AuctionId))